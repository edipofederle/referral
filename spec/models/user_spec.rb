# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:invites).class_name(ReferralInvite).with_foreign_key('inviter_id') }

  describe 'before actions' do
    it 'should initialize shareable_code_referral' do
      user = Fabricate(:user)
      expect(user.shareable_code_referral).to_not be_nil
    end
  end

  describe '#add_credit!' do
    let(:user) { Fabricate(:user) }

    it 'add credit to the user' do
      user.add_credit!(10)
      expect(user.credit).to eq 10
    end

    it 'increment credit to the user' do
      user.add_credit!(10)
      user.add_credit!(10)
      expect(user.credit).to eq 20
    end
  end

  describe '#add_credit_inviter_if_needed!' do
    let(:user) { Fabricate(:user) }
    context 'when five users already signup' do
      before do
        5.times do
          Fabricate(:referral_invite, inviter_id: user.id)
        end
      end

      it 'add credit to user' do
        user.add_credit_inviter_if_needed!(10)
        expect(user.credit).to eq 10
      end

      it 'increment credit to the user' do
        user.add_credit_inviter_if_needed!(10)
        user.add_credit_inviter_if_needed!(10)
        expect(user.credit).to eq 20
      end
    end
  end
end
