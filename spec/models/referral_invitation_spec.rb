# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReferralInvitation, type: :model do
  it { should belong_to(:inviter).class_name(User).with_foreign_key('inviter_id') }

  describe '#already_registred?' do
    let!(:existing_user) { Fabricate(:user, email: 'jhon@test.com', name: 'secret') }

    let!(:referral_invitation) do
      Fabricate(:referral_invitation, inviter: existing_user)
    end

    context 'when user already registred from a referral link' do
      before do
        Fabricate(:user, referral_identifier: referral_invitation.identifier)
      end

      it do
        expect(referral_invitation.already_registred?).to be_truthy
      end
    end

    context 'when user not yet registred from a referral link' do
      it do
        expect(referral_invitation.already_registred?).to be_falsy
      end
    end
  end
end
