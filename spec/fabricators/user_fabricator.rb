# frozen_string_literal: true

require 'faker'

Fabricator(:user) do
  email { Faker::Internet.email }
  name { Faker::Name.name }
  password { 'secret' }
end
