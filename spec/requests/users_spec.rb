# frozen_string_literal: true

require 'rails_helper'

module Api
  module V1
    RSpec.describe 'Users', type: :request do
      let!(:users) do
        Fabricate.times(2, :user)
      end

      describe 'GET #index' do
        context 'return all users' do
          it 'returns all users' do
            get api_v1_users_path
            expect(response.body).to eq users.to_json
          end
        end

        context 'search by email' do
          it 'return matched user' do
            get api_v1_users_path, params: { query: users.first.email }
            expect(response.body).to eq users.first.to_json
          end
        end
      end
    end
  end
end
