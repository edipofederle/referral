# frozen_string_literal: true

require 'rails_helper'

module Api
  module V1
    RSpec.describe 'Sign-up', type: :request do
      let!(:existing_user) {Fabricate(:user, email: 'jhon@test.com', name: 'secret')}

      let!(:referral_invitation) do
        Fabricate(:referral_invitation, inviter_id: existing_user.id)
      end

      describe 'POST #create' do
        context 'sign up user with referral code' do
          let(:params) do
            {
                email: 'invited_user@test.com',
                password: 'secret',
                referral_code: existing_user.shareable_code_referral,
                referral_identifier: referral_invitation.identifier
            }
          end

          subject(:create_user_with_referral_code) do
            post api_v1_user_registration_path, params: params
          end

          context 'success' do
            let(:fake_response) do
              {email: 'invited_user@test.com', referral_code: existing_user.shareable_code_referral, referral_inviter: existing_user.id.to_s}
            end

            it 'signup with referral code' do
              create_user_with_referral_code

              json_response = JSON.parse(response.body)['data']

              expect(json_response['email']).to eq 'invited_user@test.com'
              expect(json_response['referral_code']).to eq existing_user.shareable_code_referral
              expect(json_response['referral_identifier']).to eq referral_invitation.identifier
            end
          end

          context 'error' do
            context 'when invalid params' do
              let(:params) do
                {
                    email: '',
                    password: 'secret',
                    referral_code: 'rxx1',
                    referral_identifier: referral_invitation.identifier
                }
              end

              it 'do nothing' do
                create_user_with_referral_code

                json_response = JSON.parse(response.body)['data']
                expect(json_response['message']).to eq I18n.t('api.referral.params_validation')
              end
            end

            context 'when user already existing' do
              let(:already_invited_email) {'already_existed_user@test.com'}
              let!(:already_invited_user) do
                Fabricate(:user, email: already_invited_email,
                          referral_code: 'x100', credit: 10)
              end

              let(:params) do
                {
                    email: already_invited_email,
                    password: 'secret',
                    referral_code: existing_user.shareable_code_referral,
                    referral_identifier: referral_invitation.identifier
                }
              end

              it 'do nothing' do
                create_user_with_referral_code

                json_response = JSON.parse(response.body)['data']
                expect(json_response['message']).to eq I18n.t('api.referral.user_present')
              end
            end
          end
        end

        context 'sign up normal users' do
          subject(:create_normal_user) do
            post api_v1_user_registration_path, params: params
          end

          context 'with invalid params' do
            context 'without email and password' do
              let(:params) do
                {
                    email: '',
                    password: ''
                }
              end

              it 'returns unprocessable entity http status' do
                create_normal_user
                expect(response).to have_http_status(:unprocessable_entity)
              end

              it 'returns no uids' do
                create_normal_user

                json_response = JSON.parse(response.body)['data']

                expect(json_response['uid']).to be_blank
              end
            end

            context 'without email' do
              let(:params) do
                {
                    email: '',
                    password: 'secret'
                }
              end

              it 'returns unprocessable entity http status' do
                create_normal_user
                expect(response).to have_http_status(:unprocessable_entity)
              end
            end

            context 'without password' do
              let(:params) do
                {
                    email: 'test@test.com',
                    password: ''
                }
              end

              it 'returns unprocessable entity http status' do
                create_normal_user
                expect(response).to have_http_status(:unprocessable_entity)
              end
            end

          end

          context 'with valid params' do
            let(:params) do
              {
                  email: 'test@test.com',
                  password: 'secret'
              }
            end

            it 'log in user' do
              create_normal_user

              expect(response).to have_http_status(:ok)
              json_response = JSON.parse(response.body)['data']

              expect(json_response['uid']).to eq 'test@test.com'
            end
          end
        end
      end
    end
  end
end
