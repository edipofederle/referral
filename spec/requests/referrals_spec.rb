# frozen_string_literal: true

require 'rails_helper'

module Api
  module V1
    RSpec.describe 'Referrals', type: :request do
      describe 'POST #create' do
        subject(:create_referral) { post api_v1_referrals_path, params: params }

        context 'with existing user' do
          let!(:existing_user) { Fabricate(:user) }

          let(:params) { { inviter_id: existing_user.shareable_code_referral } }

          it 'return the shareable sign-up link' do
            create_referral

            identifier = ReferralInvitation.where(inviter_id: existing_user.id).first.identifier
            json_response = JSON.parse(response.body)['data']
            response = { 'inviter_identifier' => identifier, 'referral_code' => existing_user.shareable_code_referral, 'status' => 'ok' }
            expect(json_response['link']).to eq(response)
          end

          it 'creates the referral' do
            expect { create_referral }.to change(ReferralInvitation, :count).by(1)
            expect(ReferralInvitation.last.inviter).to eq existing_user
          end
        end

        context 'with not existing user' do
          let(:params) { { inviter_id: -1 } }

          it 'return error message' do
            create_referral
            json_response = JSON.parse(response.body)['data']
            response = {'status' => 'nok', 'error' => 'User dont exist' }
            expect(json_response).to eq(response)
          end
        end
      end
    end
  end
end
