# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateReferralInviterService do
  let!(:existing_user) { Fabricate(:user, email: 'jhon@test.com') }

  context 'when params not present' do
    it 'no params passed' do
      params = {}
      response = described_class.new(params).call
      expect(response).to eq ({ message: 'Need all params to signup' })
    end
  end

  context 'when params present' do
    let!(:referral_invitation) do
      Fabricate(:referral_invitation, inviter_id: existing_user.id)
    end

    it '5 people sign up using that referral, the inviter gets $10.' do
      5.times do |i|
        params = { email: "test_#{i}@test.com",
                   password: 'secret',
                   referral_identifier: referral_invitation.identifier,
                   referral_code: existing_user.shareable_code_referral }

        described_class.new(params).call

        expect(User.find_by_email(params[:email]).credit).to eq 10
        expect(User.find_by_email(params[:email]).referral_code).to eq existing_user.shareable_code_referral
      end

      existing_user.reload
      expect(existing_user.invites.count).to eq 5
      expect(existing_user.credit).to eq 10
    end
  end

  context 'when invited user already has an account' do
    let!(:already_invited_user) { Fabricate(:user, email: 'already_existed_user@test.com', referral_code: 'x100', credit: 10) }

    it do
      params = {
        email: 'already_existed_user@test.com',
        password: 'secret',
        referral_identifier: existing_user.id,
        referral_code: 'x100'
      }

      described_class.new(params).call
      expect(already_invited_user.credit).to eq 10
    end
  end
end
