# frozen_string_literal: true

class AddReferralCodeUsedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :referral_code_used, :string
  end
end
