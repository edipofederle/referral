# frozen_string_literal: true

class RenameTableReferralInviterToReferralInvite < ActiveRecord::Migration[6.0]
  def change
    rename_table :referral_inviters, :referral_invites
  end
end
