# frozen_string_literal: true

class AddUniqueIdentifiedToReferralInvitations < ActiveRecord::Migration[6.0]
  def change
    add_column :referral_invitations, :identifier, :string
  end
end
