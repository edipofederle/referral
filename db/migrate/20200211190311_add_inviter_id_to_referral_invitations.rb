# frozen_string_literal: true

class AddInviterIdToReferralInvitations < ActiveRecord::Migration[6.0]
  def change
    add_column :referral_invitations, :inviter_id, :integer
  end
end
