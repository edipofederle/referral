# frozen_string_literal: true

class AddNewFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :referral_code, :string
    add_column :users, :referral_inviter, :string
  end
end
