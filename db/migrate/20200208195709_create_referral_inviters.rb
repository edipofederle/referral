# frozen_string_literal: true

class CreateReferralInviters < ActiveRecord::Migration[6.0]
  def change
    create_table :referral_inviters do |t|
      t.integer :inviter_id

      t.timestamps
    end
  end
end
