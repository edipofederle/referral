# frozen_string_literal: true

class AddShareableCodeReferralToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :shareable_code_referral, :string
  end
end
