# frozen_string_literal: true

class RemoveReferralInviterFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :referral_inviter, :string
  end
end
