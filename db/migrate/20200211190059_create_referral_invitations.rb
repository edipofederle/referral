# frozen_string_literal: true

class CreateReferralInvitations < ActiveRecord::Migration[6.0]
  def change
    create_table :referral_invitations, &:timestamps
  end
end
