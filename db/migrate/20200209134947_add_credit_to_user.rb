# frozen_string_literal: true

class AddCreditToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :credit, :float, default: 0
  end
end
