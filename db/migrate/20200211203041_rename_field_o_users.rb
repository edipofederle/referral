# frozen_string_literal: true

class RenameFieldOUsers < ActiveRecord::Migration[6.0]
  def change
    rename_column :users, :referral_code_used, :referral_identifier
  end
end
