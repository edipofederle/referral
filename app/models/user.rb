# frozen_string_literal: true

require 'securerandom'

class User < ActiveRecord::Base
  extend Devise::Models

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :invites, foreign_key: 'inviter_id', class_name: 'ReferralInvite'

  before_create :assign_unique_code

  scope :by_email, ->(query) { where(email: query).first }

  def add_credit!(amount = 5)
    self.credit += amount
    save
  end

  def add_credit_inviter_if_needed!(amount = 5)
    if invites.count == 5
      self.credit += amount
      save
    end
  end

  def assign_unique_code
    self.shareable_code_referral = SecureRandom.uuid
  end
end
