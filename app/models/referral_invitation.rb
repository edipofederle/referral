# frozen_string_literal: true

require 'securerandom'

class ReferralInvitation < ApplicationRecord
  belongs_to :inviter, foreign_key: 'inviter_id', class_name: 'User'

  before_create :assign_identifier

  def assign_identifier
    self.identifier = SecureRandom.uuid
  end

  def already_registred?
    User.where(referral_identifier: identifier).exists?
  end
end
