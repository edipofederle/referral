# frozen_string_literal: true

require 'ostruct'

class CreateReferralInviterService
  attr_reader :params, :email, :password, :referral_identifier, :params

  def initialize(params)
    @params = params
    @referral_identifier = params[:referral_identifier]
    @email = params[:email]
    @password = params[:password]
  end

  def call
    return { message: I18n.t('api.referral.user_present') } if user_already_present?
    return { message: I18n.t('api.referral.params_validation') } if not_allowed_to_create?

    invited_user = User.create(params)
    ReferralInvite.create(inviter_id: inviter_user.id)

    invited_user.add_credit!(10)
    inviter_user.add_credit_inviter_if_needed!(10)

    invited_user
  end

  private

  def user_already_present?
    User.find_by_email(email)
  end

  def not_allowed_to_create?
    referral_identifier.blank? || email.blank? || password.blank?
  end

  def inviter_user
    @inviter_user ||= ReferralInvitation.where(identifier: referral_identifier).first.inviter
  end
end
