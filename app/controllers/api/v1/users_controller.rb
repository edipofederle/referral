# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController
      def index
        if query = params[:query]
          render json: User.by_email(query)
        else
          render json: User.all
        end
      end
    end
  end
end
