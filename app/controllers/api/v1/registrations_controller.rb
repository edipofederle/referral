# frozen_string_literal: true

module Api
  module V1
    class RegistrationsController < DeviseTokenAuth::RegistrationsController
      def create
        if is_normal_user?
          super
          return
        end

        response = CreateReferralInviterService.new(sign_up_params).call

        render json: { data: response }
      end

      private

      def is_normal_user?
        sign_up_params[:referral_code].blank? || sign_up_params[:referral_identifier].blank?
      end

      def sign_up_params
        params.permit(:name, :email, :password, :password_confirmation, :referral_code, :referral_identifier)
      end
    end
  end
end
