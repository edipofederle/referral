# frozen_string_literal: true

module Api
  module V1
    class ReferralsController < ApplicationController
      def create
        if user = User.where(shareable_code_referral: params[:inviter_id]).first
          referral_invitation = ReferralInvitation.create(inviter_id: user.id)

          response = {
              status: 'ok',
              inviter_identifier: referral_invitation.identifier,
              referral_code: user.shareable_code_referral
          }
          render json: {data: {link: response}}
        else
          render json: {data: {status: 'nok', error: I18n.t('api.referral.user_dont_exist')}}
        end
      end
    end
  end
end
